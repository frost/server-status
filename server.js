let http = require("http")
let https = require("https")
let tls = require("tls") // for getting the default certs
let serveStatic = require("./serve-static.js")
let path = require("path")
let fs = require("fs").promises
let fsSync = require("fs")
let util = require("util") // to promisify stuff!
let child_process = require("child_process") // for running ping with

// read list of services
let supportedServices = {}
{
	try {
		let servicesJSON = fsSync.readFileSync("services.json")
		supportedServices = JSON.parse(servicesJSON)
	} catch (error) {
		console.error(String(error))
		if (error.code == "ENOENT") {
			console.log("\nCopy services.json.example to services.json and edit it for your own services.")
			console.log("Or you might be running the server from the wrong directory.\n")
			process.exit(1)
		}
	}
}

let port = process.argv[2]
if (!port) {
	console.error("Please give a port as an argument!")
	process.exit(1)
}

async function handleRequest(request, response) {
	let url = new URL(request.url, `http://${request.headers.host}`)
	if (url.pathname === "/services") {
		// copy it so the redaction doesn't, uh, mess US up
		let services = JSON.parse(JSON.stringify(supportedServices))
		for (service in services) {
			// don't give the client our internal IPs!
			// not that it really matters, but still
			services[service] = {"name": services[service].name}
		}
		response.statusCode = 200
		response.setHeader("Content-Type", "application/json")
		response.end(JSON.stringify(services, null, "\t"))
		return
	}
	if (url.pathname.startsWith("/check/")) {
		// this is a status check
		return await checkStatus(request, response)
	}
	try {
		let content = await serveStatic.getContent("content", request)
		if (String(content).startsWith("<!DOCTYPE html>")) {
			// template it!
			content = await templateReplace(String(content))
		}
		response.end(content)
	} catch (err) {
		console.log(err)
		response.writeHead((err.code === "ENOENT") ? 404 : 500)
		response.end(`${err.message}\n`)
	}
}

async function templateReplace(content) {
	let templateRegex = /{{(.*)}}/g
	let match = undefined
	while ((match = templateRegex.exec(content)) != null) {
		// console.log(match)
		let filename = path.normalize(match[1])
		let templateContents = String(await fs.readFile(path.join("template/", filename)))
		// trim it to not mess up indent
		templateContents = templateContents.trim()
		// console.log("Template contents:", templateContents)
		
		let index = match.index
		let length = match[0].length
		content = content.substring(0, index) + templateContents + content.substring(index + length)
	}
	return content
}

async function checkStatus(request, response) {
	let url = new URL(request.url, `http://${request.headers.host}`)
	console.log("Server status check:", url.pathname)
	let serviceName = url.pathname.replace(/^\/check\//, "")
	if (serviceName === "" || serviceName === "/check") {
		// no service provided
		response.statusCode = 400
		response.end("Send a service to check, e.g. /check/foo")
		return
	}
	console.log("Checking service availability:", serviceName)
	let serviceInfo = supportedServices[serviceName]
	if (serviceInfo == null) {
		// this service doesn't exist!
		response.statusCode = 404
		response.end(serviceName + ": no such service")
		return
	}
	
	if (serviceInfo.ping) {
		checkStatusPing(serviceInfo, response)
	} else if (serviceInfo.url) {
		checkStatusHTTPS(serviceInfo, response)
	} else {
		response.statusCode = 500
		response.end(serviceName + ": no ping or URL endpoint")
	}
}

async function checkStatusPing(serviceInfo, response) {
	let ping = child_process.spawn("ping", ["-c", "1", serviceInfo.ping], {stdio: "ignore"})
	let pingExited = 0
	ping.on("close", (exitCode) => {
		if (pingExited) {
			return // ack! we've already handled this in the error handler
		}
		pingExited = true
		console.log("ping exit code:", exitCode)
		response.statusCode = 200
		let data = {
			pingExitCode: exitCode,
			up: exitCode == 0
		}
		response.setHeader("Content-Type", "application/json")
		response.end(JSON.stringify(data))
	})
	ping.on("error", (error) => {
		pingExited = 1
		console.log("Failed to launch ping:", error)
		response.statusCode = 500
		response.end(String(error))
	})
}

async function checkStatusHTTPS(serviceInfo, response) {
	let isPlainHTTP = serviceInfo.url.startsWith("http://")
	let serviceRequest = (isPlainHTTP ? http : https).get(serviceInfo.url, (backendResponse) => {
		console.log("backend response code:", backendResponse.statusCode)
		response.statusCode = 200
		let up = false
		// deem anything 200-399 to mean up
		if (200 <= backendResponse.statusCode && backendResponse.statusCode <= 399) {
			up = true
		}
		let data = {
			statusCode: backendResponse.statusCode,
			up: up
			// response: response
		}
		response.setHeader("Content-Type", "application/json")
		response.end(JSON.stringify(data))
	}).on("error", (error) => {
		console.log("request error:", error)
		// response.statusCode = 500
		// response.end(String(error))
		let data = {
			up: false,
			error: error
		}
		response.setHeader("Content-Type", "application/json")
		response.end(JSON.stringify(data))
	})
}

// trust our own root CA
async function addRootCAs() {
	https.globalAgent.options.ca = tls.rootCertificates.join("\n")
	try {
		let certFiles = await fs.readdir("certificates", {withFileTypes: true})
		certFiles = certFiles.filter((file) => {
			if (file.isDirectory()) {
				return false
			}
			return file.name.endsWith(".crt")
		})
		for (file of certFiles) {
			console.log("Trusting", file.name)
			let contents = await fs.readFile("certificates/" + file.name)
			contents = String(contents)
			https.globalAgent.options.ca += "\n" + contents
		}
	} catch (error) {
		console.warn("Couldn't load custom root CAs: " + error)
	}
}
addRootCAs()

let server = http.createServer(handleRequest)
server.listen(port, () => {
	console.log("Listening on port", port)
})

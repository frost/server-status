# Server Status Tool

This is a server to check the status of any other services you might want to keep track of. It's oriented towards something running on a proxy for everything else, with one giant backend connection that might go down (self-hosting), but if you're not set up like that it should be relatively easy to remove.

It can check services by ping or by HTTPS request. It also supports custom root certificates for if you're running off-grid.

## Dependencies

- node.js
- ping (if you have any servers that you need to check by ping)

That's literally it. It doesn't use any NPM modules.

## Installation

- Clone the repository somewhere.
- Create services.json and add your services.
- Find some way to run the thing (it needs to be run from its install directory or things'll break).

## License

Public domain! See COPYING.md for more information.

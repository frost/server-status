async function checkServerStatus(server) {
	let serverStatusIndicator = document.getElementById("status-" + server)
	serverStatusIndicator.classList.add("checking")
	serverStatusIndicator.classList.remove("ok")
	serverStatusIndicator.classList.remove("warn")
	serverStatusIndicator.classList.remove("down")
	serverStatusIndicator.removeAttribute("title") // reset any tooltips
	serverStatusIndicator.textContent = "Checking..."
	try {
		let serverStatusResponse = await fetch("/check/" + server)
		let serverStatus = await serverStatusResponse.json()
		console.log(server + ": " + (serverStatus.up ? "up" : "down"))
		if (serverStatus.up) {
			serverStatusIndicator.classList.remove("checking")
			serverStatusIndicator.classList.add("ok")
			serverStatusIndicator.textContent = "Online"
			// serverStatusIndicator.title = "This service is fully operational."
		} else {
			serverStatusIndicator.classList.remove("checking")
			serverStatusIndicator.classList.add("down")
			serverStatusIndicator.textContent = "Offline"
			// serverStatusIndicator.title = "This service is down for some reason. It could be broken, it could be someone forgot to start it, or it could be maintenance."
		}
	} catch (error) {
		console.log(error)
		serverStatusIndicator.classList.remove("checking")
		serverStatusIndicator.classList.add("warn")
		serverStatusIndicator.textContent = "Error"
		serverStatusIndicator.title = "There was an error checking the server status."
	}
}

async function buildServiceTable() {
	let serviceListResponse = await fetch("/services")
	let serviceList = await serviceListResponse.json()
	console.log(serviceList)
	
	let table = document.getElementById("services-table")
	
	for (service in serviceList) {
		if (service == "backend") {
			// already covered in the header
			continue
		}
		
		let serviceInfo = serviceList[service]
		
		let row = document.createElement("tr")
		
		let nameCell = document.createElement("td")
		nameCell.classList.add("service")
		nameCell.textContent = serviceInfo.name
		let statusCell = document.createElement("td")
		statusCell.id = "status-" + service
		statusCell.classList.add("status")
		// statusCell.classList.add("checking")
		statusCell.textContent = "--"
		
		row.appendChild(nameCell)
		row.appendChild(statusCell)
		table.appendChild(row)
		
		checkServerStatus(service)
	}
}

function checkAllServers() {
	checkServerStatus("backend")
	
	let table = document.getElementById("services-table")
	let rows = table.children
	for (row of rows) {
		let statusCell = row.children[1]
		let serviceName = statusCell.id.replace(/^status-/, "")
		checkServerStatus(serviceName)
	}
}

checkServerStatus("backend")
buildServiceTable()

setInterval(checkAllServers, 60000)
